/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.PlaceBetTO;
import ur.rzeszow.zk.bettingapp.mappings.Match;
import ur.rzeszow.zk.bettingapp.mappings.ResultEnum;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.BetRepository;
import ur.rzeszow.zk.bettingapp.repositiories.MatchRepository;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@RunWith(SpringRunner.class)
public class BetServiceTest {

    @TestConfiguration
    static class BetServiceTestContextConfiguration {

        @Bean
        public BetService betService() {
            return new BetService();
        }
    }
    @Autowired
    BetService betService;

    @MockBean
    private UserFundsRepository userFundsRepository;
    @MockBean
    BetRepository betRepository;
    @MockBean
    MatchRepository matchRepository;

    @Before
    public void setUp() {
        UserFunds u = new UserFunds(1, BigDecimal.ONE);

        Mockito.when(userFundsRepository.findById(0)).thenReturn(Optional.empty());
        Mockito.when(userFundsRepository.findById(1)).thenReturn(Optional.of(u));

        Mockito.when(matchRepository.findById(1)).thenReturn(Optional.empty());
        Match startedMatch = new Match();
        startedMatch.setId(2);
        startedMatch.setStartDate(LocalDateTime.now().minusDays(1));
        Mockito.when(matchRepository.findById(2)).thenReturn(Optional.of(startedMatch));

        Match canceled = new Match();
        canceled.setId(3);
        canceled.setStartDate(LocalDateTime.MAX);
        canceled.setCanceled(Boolean.TRUE);
        Mockito.when(matchRepository.findById(3)).thenReturn(Optional.of(canceled));
    }

    @Test
    public void testIfUserNotExist() {
        PlaceBetTO bet = new PlaceBetTO();
        bet.setUserId(0);
        RuntimeException assertThrows = Assertions.assertThrows(RuntimeException.class, () -> {
            System.out.println(betService);
            betService.placeBet(bet);
        });
    }

    @Test
    public void testIfUserDontHaveEnoughtFunds() {
        PlaceBetTO bet = new PlaceBetTO();
        bet.setUserId(1);
        bet.setBetValue(BigDecimal.TEN);
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            betService.placeBet(bet);
        });

        assertTrue(exception.getMessage().contains("Użytkownik nie ma wystarczających funduszy"));
    }

    @Test
    public void testIfThereIsNoMatch() {
        PlaceBetTO bet = new PlaceBetTO();
        bet.setUserId(1);
        bet.setBetValue(BigDecimal.valueOf(0.1));
        bet.setMatchId(1);
        bet.setBetResult(ResultEnum.T1_WIN);
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            betService.placeBet(bet);
        });

        assertTrue(exception.getMessage().contains("Nie znaleziono wybranego meczu"));
    }

    @Test
    public void testIfMatchIsStarted() {
        PlaceBetTO bet = new PlaceBetTO();
        bet.setUserId(1);
        bet.setBetValue(BigDecimal.valueOf(0.1));
        bet.setMatchId(2);
        bet.setBetResult(ResultEnum.T1_WIN);
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            betService.placeBet(bet);
        });

        assertTrue(exception.getMessage().contains("Wybrany mecz już się rozpoczął"));
    }

    @Test
    public void testIfMatchIsCanceled() {
        PlaceBetTO bet = new PlaceBetTO();
        bet.setUserId(1);
        bet.setBetValue(BigDecimal.valueOf(0.1));
        bet.setMatchId(3);
        bet.setBetResult(ResultEnum.T1_WIN);
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            betService.placeBet(bet);
        });

        assertTrue(exception.getMessage().contains("Mecz został anulowany"));
    }
}
