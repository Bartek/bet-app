/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.ChangeCourseTO;
import ur.rzeszow.zk.bettingapp.dto.MatchCreateTO;
import ur.rzeszow.zk.bettingapp.mappings.Match;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.BetRepository;
import ur.rzeszow.zk.bettingapp.repositiories.MatchRepository;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@RunWith(SpringRunner.class)
public class MatchServiceTest {
    
    @TestConfiguration
    static class MatchServiceTestContextConfiguration {

        @Bean
        public MatchService matchService() {
            return new MatchService();
        }
    }
    @Autowired
    MatchService matchService;
    
    @MockBean
    private UserFundsRepository userFundsRepository;
    @MockBean
    MatchRepository matchRepository;
    
    @Before
    public void setUp() {
        UserFunds u = new UserFunds(1, BigDecimal.ONE);

        Mockito.when(userFundsRepository.findById(0)).thenReturn(Optional.empty());
        Mockito.when(userFundsRepository.findById(1)).thenReturn(Optional.of(u));

        Mockito.when(matchRepository.findById(1)).thenReturn(Optional.empty());
        Match startedMatch = new Match();
        startedMatch.setId(2);
        startedMatch.setStartDate(LocalDateTime.now().minusDays(1));
        Mockito.when(matchRepository.findById(2)).thenReturn(Optional.of(startedMatch));

        Match canceled = new Match();
        canceled.setId(3);
        canceled.setStartDate(LocalDateTime.MAX);
        canceled.setCanceled(Boolean.TRUE);
        Mockito.when(matchRepository.findById(3)).thenReturn(Optional.of(canceled));
        
        Match withScore = new Match();
        withScore.setId(4);
        withScore.setFtGoal(123);
        withScore.setStGoal(123);
        Mockito.when(matchRepository.findById(4)).thenReturn(Optional.of(withScore));
    }
    
    
    @Test
    public void createMatchTest(){
        MatchCreateTO match = new MatchCreateTO();
        BetAppException ex = Assertions.assertThrows(BetAppException.class, () -> {
            matchService.createMatch(match);
        });
        
        assertTrue(ex.getMessage().contains("Zła wartość kursu"));
        
        match.setDrawCourse(BigDecimal.ONE);
        match.setT1WinCourse(BigDecimal.ONE);
        match.setT2WinCourse(BigDecimal.ONE);
        match.setFirstTeam("");
        match.setSecondTeam("");
        
        ex = Assertions.assertThrows(BetAppException.class, () -> {
            matchService.createMatch(match);
        });
        
        assertTrue(ex.getMessage().contains("Wprowadź nazwy drużyn"));
        
        match.setFirstTeam("dsad");
        match.setSecondTeam("fds");
        match.setStartDate(LocalDateTime.MIN);
        
        ex = Assertions.assertThrows(BetAppException.class, () -> {
            matchService.createMatch(match);
        });
        
        assertTrue(ex.getMessage().contains("Data meczu musi być późniejsza od obecnej"));
    }
    
    @Test
    public void testChangeCourse(){
        ChangeCourseTO course = new ChangeCourseTO();
        course.setId(3);
        BetAppException ex = Assertions.assertThrows(BetAppException.class, () -> {
            matchService.changeCourse(course);
        });
        
        assertTrue(ex.getMessage().contains("Mecz został anulowany"));
        
        course.setId(2);
        ex = Assertions.assertThrows(BetAppException.class, () -> {
            matchService.changeCourse(course);
        });
        
        assertTrue(ex.getMessage().contains("Zła wartość kursu"));
        
        course.setDrawCourse(BigDecimal.ONE);
        course.setT1WinCourse(BigDecimal.ONE);
        course.setT2WinCourse(BigDecimal.ONE);
        
        matchService.changeCourse(course);
    }
    
    @Test
    public void testSetMatchResult(){
    }
}
