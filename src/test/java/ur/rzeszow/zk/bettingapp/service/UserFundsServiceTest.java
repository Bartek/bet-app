/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.math.BigDecimal;
import java.util.Optional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.FundsTransferTO;
import ur.rzeszow.zk.bettingapp.dto.PayOutTO;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@RunWith(SpringRunner.class)
public class UserFundsServiceTest {
    
    @TestConfiguration
    static class UserFundsTestContextConfiguration {

        @Bean
        public UserFundsService userFundsService() {
            return new UserFundsService();
        }
    }
    
    @Autowired
    UserFundsService userFundsService;
    
    @MockBean
    private UserFundsRepository userFundsRepository;
    
    @Before
    public void setUp() {
        UserFunds u = new UserFunds(1, BigDecimal.ONE);
        Mockito.when(userFundsRepository.findById(1)).thenReturn(Optional.of(u));
    }
    
    @Test
    public void testCreditCardNumber(){
        FundsTransferTO tr = new FundsTransferTO();
        tr.setCreditCardNumber("12312");
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            userFundsService.payIn(tr);
        });

        assertTrue(exception.getMessage().contains("Błędne dane karty kredytowej"));
    }
    
    @Test
    public void testPayInNegativeFunds(){
        FundsTransferTO tr = new FundsTransferTO();
        tr.setCreditCardNumber("1234123412341234");
        tr.setValue(BigDecimal.valueOf(-1));
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            userFundsService.payIn(tr);
        });

        assertTrue(exception.getMessage().contains("Błędna ilość wpłacanych środków"));
    }
    
    @Test
    public void testWrongAcountNumber(){
        PayOutTO p = new PayOutTO();
        p.setAcountNumber("123");
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            userFundsService.payOut(p);
        });

        assertTrue(exception.getMessage().contains("Zły numer konta bankowego"));
    }
    
    @Test
    public void testNotEnoughtUserFunds(){
        PayOutTO p = new PayOutTO();
        p.setAcountNumber("1234123412341234123412341234");
        p.setUserId(1);
        p.setValue(BigDecimal.TEN);
        BetAppException exception = Assertions.assertThrows(BetAppException.class, () -> {
            userFundsService.payOut(p);
        });

        assertTrue(exception.getMessage().contains("Użytkownik nie ma wystarczających funduszy"));
    }
}
