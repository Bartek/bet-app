/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.repositiories;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ur.rzeszow.zk.bettingapp.mappings.Match;

/**
 *
 * @author zabek
 */
public interface MatchRepository extends CrudRepository<Match, Integer>{
    public List<Match> findByStartDateAfterAndCanceledIsFalse(LocalDateTime date);
    public List<Match> findNotSetedMatchesByFtGoalIsNullAndStartDateBeforeAndCanceledIsFalse(LocalDateTime startDate);
}
