/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.repositiories;

import org.springframework.data.repository.CrudRepository;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;

/**
 *
 * @author zabek
 */
public interface UserFundsRepository extends CrudRepository<UserFunds, Integer>{
    
}
