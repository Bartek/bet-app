/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.dto;

import lombok.Data;
import lombok.ToString;

/**
 *
 * @author zabek
 */
@Data
@ToString
public class MatchResultTO {
    private Integer matchId;
    private Integer ftGoal;
    private Integer stGoal;
}
