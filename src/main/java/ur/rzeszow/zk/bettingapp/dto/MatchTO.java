/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import ur.rzeszow.zk.bettingapp.mappings.Match;

/**
 *
 * @author zabek
 */
@Data
@NoArgsConstructor
public class MatchTO {
    private int id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;
    private String firstTeam;
    private String secondTeam;
    private Integer ftGoal;
    private Integer stGoal;
    private Boolean canceled;
    private BigDecimal t1WinCourse;
    private BigDecimal t2WinCourse;
    private BigDecimal drawCourse;

    public MatchTO(Match match) {
        this.id = match.getId();
        this.startDate = match.getStartDate();
        this.firstTeam = match.getFirstTeam();
        this.secondTeam = match.getSecondTeam();
        this.ftGoal = match.getFtGoal();
        this.stGoal = match.getStGoal();
        this.canceled = match.getCanceled();
        this.t1WinCourse = match.getT1WinCourse();
        this.t2WinCourse = match.getT2WinCourse();
        this.drawCourse = match.getDrawCourse();
    }
    
    
}
