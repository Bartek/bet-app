/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.dto;

import lombok.Data;

/**
 *
 * @author zabek
 */
@Data
public class CancelMatchTO {
    Integer matchId;
}   
