/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.dto;

import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @author zabek
 */
@Data
public class ChangeCourseTO {
    private Integer id;
    private BigDecimal t1WinCourse;
    private BigDecimal t2WinCourse;
    private BigDecimal drawCourse;
}
