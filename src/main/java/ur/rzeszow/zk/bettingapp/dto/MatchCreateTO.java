/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ur.rzeszow.zk.bettingapp.mappings.Match;

/**
 *
 * @author zabek
 */
@Data
@NoArgsConstructor
@ToString
public class MatchCreateTO {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;
    private String firstTeam;
    private String secondTeam;
    private BigDecimal t1WinCourse;
    private BigDecimal t2WinCourse;
    private BigDecimal drawCourse;
}
