/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.mappings;

import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author zabek
 */
@Entity
@Table(name="bet")
@Data
public class Bet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "bet_value")
    private BigDecimal betValue;
    
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name="match_id", nullable=false)
    private Match match;
    
    @Column(name = "course")
    private BigDecimal course;
    
    @Column(name = "user_id")
    private Integer userId;
    
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false, insertable = false, updatable = false)
    private UserFunds userFunds;
    
    @Column(name = "bet_winner")
    @Enumerated(EnumType.STRING)
    private ResultEnum betResult;
    
    public BigDecimal expectedWin(){
        return betValue.add(betValue.multiply(course));
    }
}
