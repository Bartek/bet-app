/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.mappings;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author zabek
 */
@Entity
@Table(name="Match")
@Data
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "start_date")
    private LocalDateTime startDate;
    
    @Column(name = "first_team")
    private String firstTeam;
    
    @Column(name = "second_team")
    private String secondTeam;
    
    @Column(name = "ft_goal")
    private Integer ftGoal;
    
    @Column(name = "st_goal")
    private Integer stGoal;
    
    @Column(name = "canceled")
    private Boolean canceled = false;
    
    @Column(name = "t1_win_course")
    private BigDecimal t1WinCourse;
    
    @Column(name = "t2_win_course")
    private BigDecimal t2WinCourse;
    
    @Column(name = "draw_course")
    private BigDecimal drawCourse;
    
    @OneToMany(mappedBy = "match", cascade = CascadeType.REFRESH)
    private Set<Bet> bets;
    
    public ResultEnum getResult(){
        if(stGoal == null){
            return null;
        }
        if(stGoal > ftGoal){
            return ResultEnum.T2_WIN;
        }
        if(stGoal == ftGoal){
            return ResultEnum.DRAW;
        }
        
        return ResultEnum.T1_WIN;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Match other = (Match) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
}
