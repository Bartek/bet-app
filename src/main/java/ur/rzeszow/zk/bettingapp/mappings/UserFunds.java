/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.mappings;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author zabek
 */
@Entity
@Table(name="user_funds")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFunds implements Serializable {
    @Id
    @Column(name = "user_id")
    private int userId;
    
    @Column(name = "funds")
    private BigDecimal funds;

    public UserFunds(int userId) {
        this.userId = userId;
        funds = BigDecimal.ZERO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.userId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserFunds other = (UserFunds) obj;
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }
    
    
}
