/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp;


/**
 *
 * @author zabek
 */
public class BetAppException extends RuntimeException{
    public BetAppException() {
    }

    public BetAppException(String message) {
        super(message);
    }

    public BetAppException(String message, Throwable cause) {
        super(message, cause);
    }
}
