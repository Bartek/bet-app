/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.PlaceBetTO;
import ur.rzeszow.zk.bettingapp.mappings.Bet;
import ur.rzeszow.zk.bettingapp.mappings.Match;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.BetRepository;
import ur.rzeszow.zk.bettingapp.repositiories.MatchRepository;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@Service
@Transactional
public class BetService {
    @Autowired
    BetRepository betRepository;
    @Autowired
    UserFundsRepository userFundsRepository;
    @Autowired
    MatchRepository matchRepository;
    
    public void placeBet(PlaceBetTO bet){
        UserFunds userFunds = userFundsRepository.findById(bet.getUserId()).orElseThrow(() -> new RuntimeException("Użytkownik nie ma żadnych funduszy"));
        if(bet.getBetValue().compareTo(userFunds.getFunds()) == 1){
            throw new BetAppException("Użytkownik nie ma wystarczających funduszy");
        }
        
        if(bet.getBetResult() == null){
            throw new BetAppException("Użtkownik nie wybrał opcji wyniku");
        }
        
        Match match = matchRepository.findById(bet.getMatchId()).orElseThrow(() -> new BetAppException("Nie znaleziono wybranego meczu"));
        if(match.getStartDate().isBefore(LocalDateTime.now())){
            throw new BetAppException("Wybrany mecz już się rozpoczął");
        }
        if(match.getCanceled()){
            throw new BetAppException("Mecz został anulowany");
        }
        
        userFunds.setFunds(userFunds.getFunds().subtract(bet.getBetValue()));
        userFundsRepository.save(userFunds);
        
        Bet b = new Bet();
        b.setMatch(match);
        b.setUserId(bet.getUserId());
        b.setBetValue(bet.getBetValue());
        switch(bet.getBetResult()){
            case DRAW:
                b.setCourse(match.getDrawCourse());
                break;
                
            case T1_WIN:
                b.setCourse(match.getT1WinCourse());
                break;
                
            case T2_WIN:
                b.setCourse(match.getT2WinCourse());
                break;
        }
        b.setBetResult(bet.getBetResult());
        betRepository.save(b);
    }
}
