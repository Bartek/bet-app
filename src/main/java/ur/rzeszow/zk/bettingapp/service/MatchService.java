/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.ChangeCourseTO;
import ur.rzeszow.zk.bettingapp.dto.MatchCreateTO;
import ur.rzeszow.zk.bettingapp.dto.MatchResultTO;
import ur.rzeszow.zk.bettingapp.dto.MatchTO;
import ur.rzeszow.zk.bettingapp.mappings.Bet;
import ur.rzeszow.zk.bettingapp.mappings.Match;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.MatchRepository;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@Service
@Transactional
public class MatchService {
    @Autowired
    MatchRepository matchRepository;
    @Autowired
    UserFundsRepository userFundsRepository;
    
    public void createMatch(MatchCreateTO match){
        if(match.getDrawCourse() == null || match.getDrawCourse().doubleValue() <= 0.0
                || match.getT1WinCourse()== null || match.getT1WinCourse().doubleValue() <= 0.0
                || match.getT2WinCourse()== null || match.getT2WinCourse().doubleValue() <= 0.0){
            throw new BetAppException("Zła wartość kursu");
        }
        if(match.getFirstTeam().isEmpty() || match.getSecondTeam().isEmpty()){
            throw new BetAppException("Wprowadź nazwy drużyn");
        }
        if(match.getStartDate().isBefore(LocalDateTime.now())){
            throw new BetAppException("Data meczu musi być późniejsza od obecnej");
        }
        
        
        Match m = new Match();
        m.setFirstTeam(match.getFirstTeam());
        m.setSecondTeam(match.getSecondTeam());
        m.setT1WinCourse(match.getT1WinCourse());
        m.setT2WinCourse(match.getT2WinCourse());
        m.setDrawCourse(match.getDrawCourse());
        m.setStartDate(match.getStartDate());
        
        matchRepository.save(m);
    }
    
    public List<MatchTO> findActiveMatches(){
        return matchRepository.findByStartDateAfterAndCanceledIsFalse(LocalDateTime.now())
                .stream().map(m->new MatchTO(m))
                .collect(Collectors.toList());
    }
    
    public List<MatchTO> findMatchesToPlaceResult(){
        return matchRepository.findNotSetedMatchesByFtGoalIsNullAndStartDateBeforeAndCanceledIsFalse(LocalDateTime.now())
                .stream().map(m->new MatchTO(m))
                .collect(Collectors.toList());
    }
    
    public void changeCourse(ChangeCourseTO courses){
        Match match = matchRepository.findById(courses.getId()).orElseThrow(()->new BetAppException("Nie znaleziono wybranego meczu"));
        if(match.getCanceled()){
            throw new BetAppException("Mecz został anulowany");
        }
        if (courses.getDrawCourse() == null || courses.getDrawCourse().doubleValue() <= 0.0
                || courses.getT1WinCourse() == null || courses.getT1WinCourse().doubleValue() <= 0.0
                || courses.getT2WinCourse() == null || courses.getT2WinCourse().doubleValue() <= 0.0) {
            throw new BetAppException("Zła wartość kursu");
        }
        match.setDrawCourse(courses.getDrawCourse());
        match.setT1WinCourse(courses.getT1WinCourse());
        match.setT2WinCourse(courses.getT2WinCourse());
        matchRepository.save(match);
    }
    
    public void setMatchResult(MatchResultTO result){
        Match match = matchRepository.findById(result.getMatchId()).orElseThrow(()->new BetAppException("Nie znaleziono wybranego meczu"));
        if(match.getStGoal() != null){
            throw new BetAppException("Wybrany mecz ma obecnie ustalony wynik");
        }
        if(match.getStartDate().isAfter(LocalDateTime.now())){
            throw new BetAppException("Wybrany mecz jeszcze się nie rozpoczął");
        }
        if(match.getCanceled()){
            throw new BetAppException("Mecz został anulowany");
        }
        
        if(result.getFtGoal() == null || result.getStGoal() == null){
            throw new BetAppException("Wynik nie jest pełny");
        }
        if(result.getFtGoal() < 0 || result.getStGoal() < 0){
            throw new BetAppException("Wynik nie jest poprawny");
        }
        
        match.setFtGoal(result.getFtGoal());
        match.setStGoal(result.getStGoal());
        matchRepository.save(match);
        
        for (Bet bet : match.getBets()) {
            if(bet.getBetResult().equals(match.getResult())){
                UserFunds userFunds = bet.getUserFunds();
                userFunds.setFunds(userFunds.getFunds().add(bet.expectedWin()));
                userFundsRepository.save(userFunds);
            }
        }
    }
    
    public void cancelMatch(Integer matchId){
        Match match = matchRepository.findById(matchId).orElseThrow(()->new BetAppException("Nie znaleziono wybranego meczu"));
        if(match.getCanceled()){
            throw new BetAppException("Ten mecz został już anulowany");
        }
        if(match.getStGoal() != null){
            throw new BetAppException("Nie można anulować zakończonego meczu");
        }
        
        match.setCanceled(true);
        matchRepository.save(match);
    }
}
