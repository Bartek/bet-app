/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.service;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.FundsTransferTO;
import ur.rzeszow.zk.bettingapp.dto.PayOutTO;
import ur.rzeszow.zk.bettingapp.mappings.UserFunds;
import ur.rzeszow.zk.bettingapp.repositiories.UserFundsRepository;

/**
 *
 * @author zabek
 */
@Service
@Transactional
public class UserFundsService {
    @Autowired
    UserFundsRepository userFundsRepository;
    
    public void payIn(FundsTransferTO transfer){
        if(transfer.getCreditCardNumber() == null || transfer.getCreditCardNumber().length() != 16){
            throw new BetAppException("Błędne dane karty kredytowej");
        }
        if(transfer.getValue().doubleValue() <= 0.0){
            throw new BetAppException("Błędna ilość wpłacanych środków");
        }
        
        UserFunds user = userFundsRepository.findById(transfer.getUserId()).orElse(new UserFunds(transfer.getUserId()));
        user.setFunds(user.getFunds().add(transfer.getValue()));
        userFundsRepository.save(user);
    }
    
    public void payOut(PayOutTO param){
        if(param.getAcountNumber() == null || param.getAcountNumber().length() != 28){
            throw new BetAppException("Zły numer konta bankowego");
        }
        
        UserFunds user = userFundsRepository.findById(param.getUserId()).orElse(null);
        if(user == null || user.getFunds().compareTo(param.getValue()) == -1){
            throw new BetAppException("Użytkownik nie ma wystarczających funduszy");
        }
        
        user.setFunds(user.getFunds().subtract(param.getValue()));
        userFundsRepository.save(user);
    }
    
    public BigDecimal getUserFunds(Integer userId){
        return userFundsRepository.findById(userId).map(u->u.getFunds()).orElse(BigDecimal.ZERO);
    }
}
