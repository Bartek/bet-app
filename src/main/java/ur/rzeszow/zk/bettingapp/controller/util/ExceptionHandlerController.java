/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.controller.util;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import ur.rzeszow.zk.bettingapp.BetAppException;

/**
 *
 * @author zabek
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @org.springframework.web.bind.annotation.ExceptionHandler(BetAppException.class)
    public ResponseEntity<String> BetError(HttpServletRequest req, Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
    
    @org.springframework.web.bind.annotation.ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> RntError(HttpServletRequest req, Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<>("Wystąpił nieoczekiwany błąd, spróbuj ponownie później.", HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<>("Wystąpił nieoczekiwany błąd, spróbuj ponownie później.", HttpStatus.BAD_REQUEST);
    }
}
