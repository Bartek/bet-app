/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.controller;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ur.rzeszow.zk.bettingapp.dto.FundsTransferTO;
import ur.rzeszow.zk.bettingapp.dto.PayOutTO;
import ur.rzeszow.zk.bettingapp.service.UserFundsService;

/**
 *
 * @author zabek
 */
@RequestMapping("/funds")
@RestController
@CrossOrigin(origins ="http://host.docker.internal:8081", allowedHeaders = "*")
public class UserFundsController {
    @Autowired
    UserFundsService userFundsService;
    
    @PostMapping("pay-in")
    public void payIn(@RequestBody FundsTransferTO transfer) {
        userFundsService.payIn(transfer);
    }
    
    @PostMapping("pay-out")
    public void payOut(@RequestBody PayOutTO param) {
        userFundsService.payOut(param);
    }
    
    @GetMapping("user")
    public BigDecimal getUserFunds(@RequestParam(value = "userId") Integer userId){
        return userFundsService.getUserFunds(userId);
    }
}
