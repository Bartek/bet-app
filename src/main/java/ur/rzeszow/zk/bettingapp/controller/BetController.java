/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ur.rzeszow.zk.bettingapp.dto.PlaceBetTO;
import ur.rzeszow.zk.bettingapp.service.BetService;

/**
 *
 * @author zabek
 */
@RestController
@RequestMapping("/bet")
@CrossOrigin(origins ="http://host.docker.internal:8081", allowedHeaders = "*")
public class BetController {
    @Autowired
    BetService betService;
    
    @PostMapping("create")
    public void placeBet(@RequestBody PlaceBetTO bet){
        betService.placeBet(bet);
    }
}
