/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.rzeszow.zk.bettingapp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ur.rzeszow.zk.bettingapp.BetAppException;
import ur.rzeszow.zk.bettingapp.dto.CancelMatchTO;
import ur.rzeszow.zk.bettingapp.dto.ChangeCourseTO;
import ur.rzeszow.zk.bettingapp.dto.MatchCreateTO;
import ur.rzeszow.zk.bettingapp.dto.MatchResultTO;
import ur.rzeszow.zk.bettingapp.dto.MatchTO;
import ur.rzeszow.zk.bettingapp.dto.UserInfoTO;
import ur.rzeszow.zk.bettingapp.service.MatchService;

/**
 *
 * @author zabek
 */
@Controller
@RequestMapping("/match")
@RestController
@CrossOrigin(origins ="http://host.docker.internal:8081", allowedHeaders = "*")
public class MatchController {

    @Autowired
    MatchService matchService;
    
    @GetMapping("active")
    public List<MatchTO> getActiveMatches() {
        return matchService.findActiveMatches();
    }
    
    @GetMapping("ended-no-result")
    public List<MatchTO> getMatchesToPlaceResult() {
        return matchService.findMatchesToPlaceResult();
    }
    
    @PostMapping("create")
    public void createMatch(@RequestBody MatchCreateTO matchCreate, @RequestHeader(name = "user-info", required = true) String user) throws JsonProcessingException {
        checkIfUserIsAdmin(user);
        matchService.createMatch(matchCreate);
    }
    
    @PostMapping("change-course")
    public void changeCourse(@RequestBody ChangeCourseTO courses, @RequestHeader(name = "user-info", required = true) String user) throws JsonProcessingException {
        checkIfUserIsAdmin(user);
        matchService.changeCourse(courses);
    }
    
    @PostMapping("set-result")
    public void setMatchResult(@RequestBody MatchResultTO result, @RequestHeader(name = "user-info", required = true) String user) throws JsonProcessingException {
        checkIfUserIsAdmin(user);
        matchService.setMatchResult(result);
    }
    
    @PostMapping("cancel")
    public void cancelMatch(@RequestBody CancelMatchTO param, @RequestHeader(name = "user-info", required = true) String user) throws JsonProcessingException {
        checkIfUserIsAdmin(user);
        matchService.cancelMatch(param.getMatchId());
    }
    
    private void checkIfUserIsAdmin(String json) throws JsonProcessingException{
        UserInfoTO t = new ObjectMapper().readValue(json, UserInfoTO.class);
        if(!"admin".equals(t.getRole())){
            throw new BetAppException("Użytkownik nie ma wystarczających uprawnień");
        }
    }
}
